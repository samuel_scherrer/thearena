// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "Vehicles/VehicleWheel.h"
#include "TheArenaWheelRear.generated.h"

UCLASS()
class UTheArenaWheelRear : public UVehicleWheel
{
	GENERATED_BODY()

public:
	UTheArenaWheelRear(const FObjectInitializer& ObjectInitializer);
};



