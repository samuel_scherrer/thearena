// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "TheArena.h"
#include "TheArenaHud.h"
#include "TheArenaPawn.h"
#include "Engine/Canvas.h"
#include "Engine/Font.h"
#include "CanvasItem.h"
#include "2BDreamsC++Content/Effect.h"

#ifdef HMD_INTGERATION
// Needed for VR Headset
#include "Engine.h"
#include "IHeadMountedDisplay.h"
#endif // HMD_INTGERATION

#define LOCTEXT_NAMESPACE "VehicleHUD"

ATheArenaHud::ATheArenaHud(const FObjectInitializer& ObjectInitializer) 
	: Super(ObjectInitializer)
{
	static ConstructorHelpers::FObjectFinder<UFont> Font(TEXT("/Engine/EngineFonts/RobotoDistanceField"));
	HUDFont = Font.Object;
}

void ATheArenaHud::DrawHUD()
{
	Super::DrawHUD();

	// Calculate ratio from 720p
	const float HUDXRatio = Canvas->SizeX / 1280.f;
	const float HUDYRatio = Canvas->SizeY / 720.f;

	bool bHMDDeviceActive = false;

#ifdef HMD_INTGERATION
	// We dont want the onscreen hud when using a HMD device	
	if (GEngine->HMDDevice.IsValid() == true )
	{
		bHMDDeviceActive = GEngine->HMDDevice->IsStereoEnabled();
	}
#endif
	if( bHMDDeviceActive == false )
	{
		// Get our vehicle so we can check if we are in car. If we are we don't want onscreen HUD
		ATheArenaPawn* Vehicle = Cast<ATheArenaPawn>(GetOwningPawn());
		if ((Vehicle != nullptr) && (Vehicle->bInCarCameraActive == false))
		{
			FVector2D ScaleVec(HUDYRatio * 1.4f, HUDYRatio * 1.4f);

			// Speed
			FCanvasTextItem SpeedTextItem(FVector2D(HUDXRatio * 50.f, HUDYRatio * 0), Vehicle->SpeedDisplayString, HUDFont, FLinearColor::White);
			SpeedTextItem.Scale = ScaleVec;
			Canvas->DrawItem(SpeedTextItem);

			// Gear
			FCanvasTextItem GearTextItem(FVector2D(HUDXRatio * 50.f, HUDYRatio * 50.f), Vehicle->GearDisplayString, HUDFont, Vehicle->bInReverseGear == false ? Vehicle->GearDisplayColor : Vehicle->GearDisplayReverseColor);
			GearTextItem.Scale = ScaleVec;
			Canvas->DrawItem(GearTextItem);

			// Health
			FCanvasTextItem HealthTextItem(FVector2D(HUDXRatio * 50.f, HUDYRatio * 100), Vehicle->HealthDisplayString, HUDFont, FLinearColor::White);
			HealthTextItem.Scale = ScaleVec;
			Canvas->DrawItem(HealthTextItem);

			// Buff
			FCanvasTextItem EffectTextItem(FVector2D(HUDXRatio * 50.f, HUDYRatio * 200), Vehicle->BuffDisplayString, HUDFont, FLinearColor::White);
			EffectTextItem.Scale = ScaleVec;
			Canvas->DrawItem(EffectTextItem);
		}
	}
}

#undef LOCTEXT_NAMESPACE
