// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "TheArena.h"
#include "TheArenaGameMode.h"
#include "TheArenaPawn.h"
#include "TheArenaHud.h"

ATheArenaGameMode::ATheArenaGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	DefaultPawnClass = ATheArenaPawn::StaticClass();
	HUDClass = ATheArenaHud::StaticClass();
}
