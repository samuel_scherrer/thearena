// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "TheArena.h"
#include "TheArenaWheelFront.h"

UTheArenaWheelFront::UTheArenaWheelFront(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	ShapeRadius = 18.f;
	ShapeWidth = 15.0f;
	bAffectedByHandbrake = false;
	SteerAngle = 40.f;
}
