// Fill out your copyright notice in the Description page of Project Settings.

#include "TheArena.h"
#include "BasePickup.h"


// Sets default values
ABasePickup::ABasePickup()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Ao ser criado est� ativo
	isActive = true;

	//BaseCollisionComponent server para lidar com as colisoes e por isso � invisivel 
	baseCollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("rootComponent"));
	RootComponent = baseCollisionComponent;

	//Representacao visual do pickup
	baseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("baseMesh"));
	//Ligamos a fisica para esse elemento
	baseMesh->SetSimulatePhysics(true);

	//Colocamos a mesh como filha do componente base
	baseMesh->AttachTo(baseCollisionComponent);


}

// Called when the game starts or when spawned
void ABasePickup::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABasePickup::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void ABasePickup::SetMeshScale(FVector scale)
{
	baseMesh->SetRelativeScale3D(scale);
}

// ========= Custom Functions =========
//Mudamos o nome dass funcao pois ela � uma funcao nativa do blueprinte (ver as UFUNTION setadas para ela no .h)
//Dessa forma a engine primeiro tenta excutar a versao dela implementada por blueprint caso nao exista tal implementacao
//Ele ira executada essa. O nome deve ser nomeDadoPraFuncao_Implementation()
void ABasePickup::OnPickedUp_Implementation()
{
	//Implementacoes diferentes nas classes que herdarem essa
	//Destroy the batery
	Destroy();
}
