// Fill out your copyright notice in the Description page of Project Settings.

#include "TheArena.h"
#include "BuffBox.h"
#include "Effect.h"
#include "TheArenaPawn.h"

ABuffBox::ABuffBox()
{
}

void ABuffBox::OnPickedUp_Implementation()
{
	// Chamamos o metodo do pai desta classe
	Super::OnPickedUp();
}

void ABuffBox::OnHit(AActor* actor){
	
	ATheArenaPawn* player = dynamic_cast<ATheArenaPawn*>(actor);
	//So tratamos se for o jogador
	if (player != NULL)
	{
		//S� permite o pickup se o player est� com o slot de buffs livre
		if (player->GetBuffNameFromSlot() == "")
		{
			//Destruimos a caixa
			OnPickedUp_Implementation();

			//Pedimoas para a classe Effect criar um aleat�rio
			UEffect* effect = NewObject<class UEffect>();
			effect->InitWithRandomBuff(GetWorld(), player);
		}
	}
}