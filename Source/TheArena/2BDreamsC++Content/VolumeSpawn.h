// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BasePickup.h"
#include "GameFramework/Actor.h"
#include "VolumeSpawn.generated.h"

UCLASS()
class THEARENA_API AVolumeSpawn : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AVolumeSpawn();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	//Box que demarca a area possivel de se fazer spawn
	UPROPERTY(VisibleInstanceOnly, Category = Spawning)
		UBoxComponent *whereToSpawn;

	//Iniciado por default com 1, 1, 1. Caso o usu�rio n�o passe nenhum valor
	UPROPERTY(EditAnywhere, Category = Spawning)
		FVector objSpawnScale = FVector(1, 1, 1);

	//Objecto q spawn. Temos que usar uma TSubClassOf pois queremos que o usu�rio possa passar 
	//qualquer objeto baseado em um BasePickup, por exemplo um blueprint que herde da BasePickup
	UPROPERTY(EditAnywhere, Category = Spawning)
		TSubclassOf<class ABasePickup> whatToSpawn;

	//Minimo tempo de delay de spawn
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Spawning)
		float spawnDelayRangeLow;

	//Maximo tempo de delay de spawn
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Spawning)
		float spawnDelayRangeHigh;

	//Fun��o que retorna o ponto onde ser� instanciado o objeto
	UFUNCTION(BlueprintPure, Category = Spawning)
		FVector GetRandomPointInVolume();

private:

	//Tempo de delay
	float GetRandomSpawnDelay();
	
	//Delay atual
	float spawnDelay;

	//tempo para que seja feito o spawn
	float spawnTime;

	//Instancia o objeto no ponto indicado
	void SpawnPickup();
};
