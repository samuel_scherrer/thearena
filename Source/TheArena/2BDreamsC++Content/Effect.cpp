// Fill out your copyright notice in the Description page of Project Settings.

#include "TheArena.h"
#include "Effect.h"
#include "TheArenaPawn.h"

#define LOCTEXT_NAMESPACE "Effect"

void UEffect::Init(UWorld* world, float duration, BuffTypes type, bool isOverTimeEffect, ATheArenaPawn* target)
{
	//Se possui dura��o n�o � instantaneo
	if (duration != 0){
		//Verifica se tem um mundo valido
		if (world != NULL)
		{
			this->world = world;
			//Verifica se o efeito � durante um per�odo
			if (isOverTimeEffect)
			{
				//O Buff ser� aplicado a cada 1s
				FTimerHandle time_handler;
				world->GetTimerManager().SetTimer(time_handler, this, &UEffect::ApplyBuff, 1.0f, true);
			}
			//Ou se o buff ter� seu aplicado e durar� durante um tempo
			else
			{
				//O Buff ser� aplicado e ficar� o tempo demarcado pela duration
				FTimerHandle time_handler;
				world->GetTimerManager().SetTimer(time_handler, this, &UEffect::RemoveBuff, duration, true);
			}

			//Devemos aguardar as vari�veis pois o buff ir� durar um tempo
			this->duration = duration;
			this->currTime = 0;
			this->target = target;
			this->buffType = type;
			this->isOverTimeEffect = isOverTimeEffect;

			//Indicamos para o target q ele recebeu um buff
			target->SetBuff(GetEffectName(), duration);

			ApplyBuff();
		}
	}
	//Ocorre instantaneamente e pronto
	else{
		this->target = target;
		this->buffType = type;
		this->duration = 0;
		this->currTime = 0;

		//target->SetBuff(GetEffectName(), 0);

		ApplyBuff();
		this->~UEffect();
	}
}

void UEffect::InitWithRandomBuff(UWorld* world, ATheArenaPawn* target)
{
	//Sorteamos um tipo de buff
	int res = FMath::RandRange(0, totalOfEffects - 1);

	//Criamos o buff de acordo com o tipo
	switch (res)
	{
	case HEAL:
		Init(world, 0, HEAL, false, target);
		break;
	case CONT_HEAL:
		Init(world, 5, HEAL, true, target);
		break;
	case DAMAGE:
		Init(world, 0, DAMAGE, false, target);
		break;
	case CONT_DAMAGE:
		Init(world, 10, DAMAGE, true, target);
		break;
	case SPEED_UP:
		Init(world, 0, SPEED_UP, false, target);
		break;
	case SPEED_DOWN:
		Init(world, 0, SPEED_DOWN, false, target);
		break;
	case NONE:
		break;
	default:
		break;
	}
}

UEffect::~UEffect()
{
}

/*Casos:
Se o buff � instantaneo esse m�todo � chamado e a classe morre
Se o buff � overTime esse m�todo � chamado a cada 1s, durante a duration. Quando atingir a duration a classe morre
Se o buff � durante um tempo esse m�todo � chamado somente uma vez e ent�o ap�s a duration o RemoveBuff � chamado*/
void UEffect::ApplyBuff()
{
	switch (buffType)
	{
	case HEAL:
		target->UpdateHealth(HEAL_VAL);
		break;
	case DAMAGE:
		target->UpdateHealth(DAMAGE_VAL);
		break;
	case SPEED_UP:
		target->ChangeMaxRPM(SPEED_UP_VAL);
		break;
	case SPEED_DOWN:
		target->ChangeMaxRPM(SPEED_DOWN_VAL);
		break;
	default:
		break;
	}
	if (isOverTimeEffect)
	{
		if (currTime > duration)
		{
			StopBuff();
		}
		else
		{
			currTime++;
		}
	}
}

void UEffect::RemoveBuff()
{
	switch (buffType)
	{
	case SPEED_UP:
		target->ChangeMaxRPM(-1 * SPEED_UP_VAL);
		break;
	case SPEED_DOWN:
		target->ChangeMaxRPM(-1 * SPEED_DOWN_VAL);
		break;
	default:
		break;
	}
	StopBuff();
}

void UEffect::StopBuff()
{
	//Setamos o rate para zero assim para de fazer a chamada
	FTimerHandle time_handler;
	if (isOverTimeEffect)
		world->GetTimerManager().SetTimer(time_handler, this, &UEffect::ApplyBuff, 0, true);
	else
		world->GetTimerManager().SetTimer(time_handler, this, &UEffect::RemoveBuff, 0, true);
	target->SetBuff("", 0);
	this->~UEffect();
}

FString UEffect::GetEffectName()
{
	//Tem que falta para o buff terminar
	float timeLeft = duration - currTime;

	switch (buffType)
	{
	case HEAL:
		return "Heal";
	case DAMAGE:
		return "Damage";
	case SPEED_UP:
		return "Faster";
	case SPEED_DOWN:
		return "Slow";
	case NONE:
		return "Placebo";
	default:
		return "";
	}
}



