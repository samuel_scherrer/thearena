// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "2BDreamsC++Content/BasePickup.h"
#include "BuffBox.generated.h"

class BuffFactory;
/**
 * 
 */
UCLASS()
class THEARENA_API ABuffBox : public ABasePickup
{
	GENERATED_BODY()

	ABuffBox();
	//Sobreescrevenmos o metodo do pai Pickup.h
	void OnPickedUp_Implementation();

public:

	UFUNCTION(BlueprintCallable, Category = BuffBox)
	void OnHit(AActor* actor);
};
