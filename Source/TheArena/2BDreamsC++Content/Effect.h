// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "Effect.generated.h"

class ATheArenaPawn;
/**
 * 
 */
enum BuffTypes {
	HEAL,
	//Para esta classe HEAL e CONT_HEAL s�o equivalentes. Servindo somente para a classe Factory 
	//saber que deve criar esse buff com uma dura��o. O mesmo vale para o enum CONT_DAMAGE
	CONT_HEAL,
	DAMAGE,
	CONT_DAMAGE,
	SPEED_UP,
	SPEED_DOWN,
	NONE
};

UCLASS()
class THEARENA_API UEffect : public UObject
{
	GENERATED_BODY()
	
public:
	void Init(UWorld* world, float duration, BuffTypes type, bool isOverTimeEffect, ATheArenaPawn* target);
	void InitWithRandomBuff(UWorld* world, ATheArenaPawn* target);
	~UEffect();

	//Valor estatico para ser consultado por outras classes para saber a quantidade de buffs poss�veis
	static int const totalOfEffects = 7;

private:
	//Constantes
	float const HEAL_VAL = 10.0f;
	float const DAMAGE_VAL = -20.0f;
	float const SPEED_UP_VAL = 1000.0f;
	float const SPEED_DOWN_VAL = -500.0f;

	//Total de tempo que ja se passou
	float currTime;

	//Dura��o do buff
	float duration;

	//Indica se � um buff com effeito durante um tempo
	float isOverTimeEffect;

	//Tipo de buff
	BuffTypes buffType;

	//Quem recebe o buff
	ATheArenaPawn* target;

	//Mundo em que o target est�
	UWorld* world;

	//Executa a a��o do buff
	void ApplyBuff();

	//Remove o effeito do buff
	void RemoveBuff();

	//Para o timer que fica chamando o buff a cada 1s. 
	void StopBuff();
	
	//Seta o nome do efeito na vari�vel que � diposnivel para leitura
	FString GetEffectName();
};
