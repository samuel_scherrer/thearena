// Fill out your copyright notice in the Description page of Project Settings.

#include "TheArena.h"
#include "VolumeSpawn.h"


// Sets default values
AVolumeSpawn::AVolumeSpawn()
{
	//Criamos a caixa q delimita a area onde podem ser instanciados poickups
	whereToSpawn = CreateDefaultSubobject<UBoxComponent>(TEXT("whereToSpawn"));
	RootComponent = whereToSpawn;

	spawnDelayRangeLow = 1.0f;
	spawnDelayRangeHigh = 4.5f;
	spawnDelay = GetRandomSpawnDelay();

	//Vamos usar a tick function para instanciar
	//para isso precisamos ativar o tick para essa classe
	//PrimaryActorTick.bCanEverTick = true;
}

void AVolumeSpawn::SpawnPickup()
{
	//Verifica se tem um objeto para instanciar
	if (whatToSpawn != NULL)
	{
		//Verifica se tem um mundo valido
		UWorld* const world = GetWorld();
		if (world)
		{
			//Parametros para spawn
			FActorSpawnParameters spawnParams;
			spawnParams.Owner = this;
			spawnParams.Instigator = Instigator;

			//Local de spawn
			FVector spawnLocation = GetRandomPointInVolume();

			//Rota��o ao spawn
			FRotator spawnRotation;
			spawnRotation.Yaw = FMath::FRand() * 360.f;
			spawnRotation.Pitch = FMath::FRand() * 360.f;
			spawnRotation.Roll = FMath::FRand() * 360.f;

			//Setamos a scale do objeto
			whatToSpawn->GetDefaultObject<ABasePickup>()->SetMeshScale(objSpawnScale);

			//Spawn do item.
			ABasePickup* const spawnedPickup = world->SpawnActor<ABasePickup>(whatToSpawn, spawnLocation, spawnRotation, spawnParams);
		}
	}
}

float AVolumeSpawn::GetRandomSpawnDelay()
{
	return FMath::FRandRange(spawnDelayRangeLow, spawnDelayRangeHigh);
}

FVector AVolumeSpawn::GetRandomPointInVolume()
{
	FVector randomLocation;
	float minX, minY, minZ;
	float maxX, maxY, maxZ;

	FVector origin;
	FVector boxExtend;

	//Origem e extensao da caixa q demarca onde instanciar o objeto
	origin = whereToSpawn->Bounds.Origin;
	boxExtend = whereToSpawn->Bounds.BoxExtent;

	//Pegamos o min em cada eixo
	minX = origin.X - boxExtend.X / 2.0f;
	minY = origin.Y - boxExtend.Y / 2.0f;
	minZ = origin.Z - boxExtend.Z / 2.0f;

	//Pegamos o max em cada eixo
	maxX = origin.X + boxExtend.X / 2.0f;
	maxY = origin.Y + boxExtend.Y / 2.0f;
	maxZ = origin.Z + boxExtend.Z / 2.0f;

	randomLocation.X = FMath::FRandRange(minX, maxX);
	randomLocation.Y = FMath::FRandRange(minY, maxY);
	randomLocation.Z = FMath::FRandRange(minZ, maxZ);

	return randomLocation;
}

// Called when the game starts or when spawned
void AVolumeSpawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AVolumeSpawn::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	spawnTime += DeltaTime;

	if (spawnTime > spawnDelay)
	{
		SpawnPickup();

		//zeramos o spawntime e sorteamos um novo delay
		spawnTime -= spawnDelay;
		spawnDelay = GetRandomSpawnDelay();
	}
}

