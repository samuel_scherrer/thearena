// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/SaveGame.h"
#include "CustomSaveGame.generated.h"

/**
 * 
 */
UCLASS()
class THEARENA_API UCustomSaveGame : public USaveGame
{
	GENERATED_BODY()
	
public:

	UCustomSaveGame(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(VisibleAnywhere, Category = gameControl)
	bool isSinglePlayer;

	UPROPERTY(VisibleAnywhere, Category = gameControl)
	FString levelSelected;

	UPROPERTY(VisibleAnywhere, Category = gameControl)
	FString player1CarSelected;

	UPROPERTY(VisibleAnywhere, Category = gameControl)
	FString player2CarSelected;

	UPROPERTY(VisibleAnywhere, Category = Basic)
	FString saveSlotName;
	
	
};
