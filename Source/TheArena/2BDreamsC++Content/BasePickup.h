// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "BasePickup.generated.h"

UCLASS()
class THEARENA_API ABasePickup : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABasePickup();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	//Informa se o pickup esta ativo
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isActive;

	//Root component, invisivel
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly)
		USphereComponent* baseCollisionComponent;

	//Mesh do pickup
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly)
		UStaticMeshComponent* baseMesh;

	//Permite o usu�rio setar a scale do objeto.
	void SetMeshScale(FVector scale);

	//Essa propriedade diz que podemos sobreescrever esse metodo em um blueprint
	//A engine assim primeiro vai tentar usar a versao em blueprint se nao achar
	//vai usar a versao que estiver aqui ou se chamarmos ela diretamente. 
	//Devemos criar a funcao e depois uma versao
	//dela com o seguinte nome [FUNCTION NAME]_Implementation
	UFUNCTION(BlueprintNativeEvent)
		void OnPickedUp();
		void OnPickedUp_Implementation();
};
