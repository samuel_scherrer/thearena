// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "Vehicles/VehicleWheel.h"
#include "TheArenaWheelFront.generated.h"

UCLASS()
class UTheArenaWheelFront : public UVehicleWheel
{
	GENERATED_BODY()

public:
	UTheArenaWheelFront(const FObjectInitializer& ObjectInitializer);
};



